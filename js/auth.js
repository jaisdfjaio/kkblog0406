$('.btn-update').on('click', () => {
    $('.modal-user').modal('show')
})
$('.btn-update-user').on('click', () => {
    firebase.auth().currentUser.updateProfile({
        displayName: $('#user-name').val(),
        photoURL: $('#user-photo').val(),
    }).then((res) => {
        alert('更新成功')
        // Update successful.
    }).catch((error) => {
        alert('更新失敗QQ')
        // An error happened.
    });
})

$('.btn-forgot-password').on('click', () => {
    $('.modal').modal('hide')
    $('.modal-forgot-password').modal('show')
})

$('.btn-send-forgot-password').on('click', () => {
    const email = $('#forgot-password-email').val()
    firebase.auth().sendPasswordResetEmail(email)
        .then((res) => {
            alert('成功寄送重置密碼信')
            // Email sent.
        }).catch((error) => {
            alert('無法寄送重置密碼信')
            // An error happened.
        });
})
$('.btn-reset-password').on('click', () => {
    $('.modal-user-reset-password').modal('show')
    $('#password-reset-modal').on('click',
        () => {
            console.log('in func')
            const user = firebase.auth().currentUser;
            const newPassword = $('#pw-new-1').val();
            const confirmPassword = $('#pw-new-2').val();
            if (newPassword !== confirmPassword)
                alert('密碼不吻合');
            else {
                console.log(newPassword);
                user.updatePassword(newPassword).then(function () {
                    alert('成功更新密碼')
                }).catch(function (error) {
                    alert('fail更新密碼')
                });
            }
        })
})
$('.btn-fb-login').on('click', () => {
    var provider = new firebase.auth.FacebookAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function (result) {
        // This gives you a Facebook Access Token. You can use it to access the Facebook API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
        alert(`成功用fb登入${user.email} ${user.displayName}`)
    }).catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
        console.log(error.message)
        alert(`無法用fb登入`)
    });
})
$('.btn-google-login').on('click', () => {
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider).then(function (result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        const token = result.credential.accessToken;
        // The signed-in user info.
        const user = result.user;
        // ...
    }).catch(function (error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        // The email of the user's account used.
        const email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        const credential = error.credential;
        // ...
    });
})
