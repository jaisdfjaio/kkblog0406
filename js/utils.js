//有用的小工具 utilities
const updateBlog = () => {
    $('.posts').empty()
    blog.posts.forEach((post, index) => {
        const $post = $('<div class="card mb-3">').append(
            $('<div class="row no-gutters">').append(
                $('<div class="col-md-4">').append(
                    $(`<div class="image" style="background-image: url(${post.cover});">`)
                ),
                $('<div class="col-md-8">').append(
                    $('<div class="card-body">').append(
                        $('<h5 class="card-title">').html(post.title),
                        $('<p class="card-text">').html(post.content),
                        $('<p class="card-text">').append(
                            $('<small class="text-muted">').html('Last updated 3 mins ago')
                        )
                    )
                )
            )
        )
        $post.on('click', () => {
            // 更新 modal-post 裡面的資訊
            $('.modal-post .modal-title').html(post.title)
            $('.modal-post .modal-body .container').html(post.content)
            $('.modal-post .modal-body .image').css('background-image', `url(${post.cover})`)

            $('.modal-post').modal('show')
        })
        if (post.private) {
            $('.posts').append(
                $('<div class="auth col-12 col-md-6 col-lg-4">').html($post)
            )
        } else {
            $('.posts').append(
                $('<div class="col-12 col-md-6 col-lg-4">').html($post)
            )
        }
    })
}

